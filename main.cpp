/*
 * main.cpp
 *
 *  Created on: 15.03.2012
 *      Author: legath
 */


#include "stm32f10x.h"
#include <scmRTOS.h>
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_usart.h"
#include "misc.h"
//---------------------------------------------------------------------------
//
//      Process types
//
typedef OS::process<OS::pr0, 300> TProc1;
typedef OS::process<OS::pr1, 300> TProc2;
typedef OS::process<OS::pr2, 300> TProc3;

//---------------------------------------------------------------------------
//
//      Process objects
//
TProc1 Proc1;
TProc2 Proc2;
TProc3 Proc3;


//---------------------------------------------------------------------------
//
//      IO Pins
//
//---------------------------------------------------------------------------
//
//      Event Flags to test
//
OS::TEventFlag ef;
OS::TEventFlag TimerEvent;


typedef OS::channel<uint16_t, 9> Usart_channel;


Usart_channel input_buf;
Usart_channel output_buf;

OS::TMutex testmutex;
char flag=1;
void init_usart()
{
		  GPIO_InitTypeDef  GPIO_InitStructure;


	      /* Enable GPIOA clock                                                   */
	      RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_USART1, ENABLE);

	      /* Configure USART1 Rx (PA10) as input floating                         */
	      GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_10;
	      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	      GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
	      GPIO_Init(GPIOA, &GPIO_InitStructure);

	      /* Configure USART1 Tx (PA9) as alternate function push-pull            */
	      GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_9;
	      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	      GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
	      GPIO_Init(GPIOA, &GPIO_InitStructure);


	      USART_InitTypeDef USART_InitStructure;
	      USART_InitStructure.USART_BaudRate            = 115200;
	      USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
	      USART_InitStructure.USART_StopBits            = USART_StopBits_1;
	      USART_InitStructure.USART_Parity              = USART_Parity_No ;
	      USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	      USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
	      USART_Init(USART1, &USART_InitStructure);
	      USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	      NVIC_InitTypeDef InitTimIrq;
	      InitTimIrq.NVIC_IRQChannel=USART1_IRQn;
	      InitTimIrq.NVIC_IRQChannelPreemptionPriority=0;
	      InitTimIrq.NVIC_IRQChannelSubPriority=1;
	      InitTimIrq.NVIC_IRQChannelCmd=ENABLE;
	      NVIC_Init(&InitTimIrq);
	      USART_Cmd(USART1, ENABLE);

	}


void init_leds()
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8|GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    GPIO_ResetBits(GPIOC, GPIO_Pin_8);
    GPIO_ResetBits(GPIOC, GPIO_Pin_9);
	}

void pwm_init()
{
	//TIM3 generate PWM for servo and motor control
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	GPIO_InitTypeDef GPIO_Config;
    GPIO_Config.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_Config.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Config.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_Config);

    TIM_TimeBaseInitTypeDef TIM_BaseConfig;
    TIM_OCInitTypeDef TIM_OCConfig;

    TIM_BaseConfig.TIM_Prescaler = 9;
    TIM_BaseConfig.TIM_Period = 48000;
    TIM_BaseConfig.TIM_ClockDivision = 0;
    TIM_BaseConfig.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM3, &TIM_BaseConfig);

    TIM_OCConfig.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCConfig.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCConfig.TIM_Pulse = 3600;
    TIM_OCConfig.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OC1Init(TIM3, &TIM_OCConfig);

    TIM_OCConfig.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCConfig.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCConfig.TIM_Pulse = 3600;
    TIM_OCConfig.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OC2Init(TIM3, &TIM_OCConfig);

    TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);
    TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);
    TIM_ARRPreloadConfig(TIM3, ENABLE);
    TIM_Cmd(TIM3, ENABLE);


	}

void timer_init(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	TIM_TimeBaseInitTypeDef Init_info;
	TIM_DeInit(TIM2);
	/*		TIM_Period * (TIM_Prescaler +1)
	 * Time=_______________________________
	 * 				24МГц
	 *
	 * 		TIM_Prescaler=1199
	 * 		TIM_Period=20000
	 */
	Init_info.TIM_Prescaler=1199;
	Init_info.TIM_Period=20000;
	Init_info.TIM_ClockDivision=0;
	Init_info.TIM_CounterMode=TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM2,&Init_info);
	TIM_ARRPreloadConfig(TIM2,ENABLE);
	TIM_UpdateRequestConfig(TIM2, TIM_UpdateSource_Regular);
	TIM_ClearFlag(TIM2, TIM_FLAG_Update);
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
	TIM_Cmd(TIM2,ENABLE);

	NVIC_InitTypeDef InitTimIrq;
	InitTimIrq.NVIC_IRQChannel=TIM2_IRQn;
	InitTimIrq.NVIC_IRQChannelPreemptionPriority=1;
	InitTimIrq.NVIC_IRQChannelSubPriority=1;
	InitTimIrq.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&InitTimIrq);
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);




	}
int main()
{
	init_leds();
	timer_init();
	pwm_init();
	init_usart();
    // run
    OS::run();

}

namespace OS
{
    template <>
    OS_PROCESS void TProc1::exec()
    {
        for(;;)
        {
            TimerEvent.wait();

            if(GPIO_ReadOutputDataBit  ( GPIOC,GPIO_Pin_8))
                 GPIO_WriteBit(GPIOC,GPIO_Pin_8,Bit_RESET);
            else
                    GPIO_WriteBit(GPIOC,GPIO_Pin_8,Bit_SET);
            sleep(1);

        }
    }

    template <>
    OS_PROCESS void TProc2::exec()
    {
        for(;;)
        {
        	ef.wait();
            if(GPIO_ReadOutputDataBit  ( GPIOC,GPIO_Pin_9))
                 GPIO_WriteBit(GPIOC,GPIO_Pin_9,Bit_RESET);
            else
                    GPIO_WriteBit(GPIOC,GPIO_Pin_9,Bit_SET);
            sleep(20);
        }
    }
    template <>
    OS_PROCESS void TProc3::exec()
    {
        for(;;)
        {
        	if (input_buf.get_count()<3)
        		sleep(1);

        	sleep(1);
        }
    }

}

void OS::system_timer_user_hook()
{

    TimerEvent.signal_isr();
}

#if scmRTOS_IDLE_HOOK_ENABLE
void OS::idle_process_user_hook()
{
	__WFI();
}
#endif

extern "C" void TIM2_IRQHandler(void)
{
	OS::TISRW in_handler;
	TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
	ef.signal_isr();
	}
extern "C" void USART1_IRQHandler(void)
{
	OS::TISRW in_handler;
	USART_ClearITPendingBit(USART1, USART_IT_RXNE);
	input_buf.push(USART_ReceiveData(USART1));

	}
